<%-- 
    Document   : MCSDError
    Created on : Mar 6, 2018, 2:44:46 PM
    Author     : sdunham1231
--%>

<jsp:include page="MCSDBanner.jsp" />

    <body>

<h1>Java Error</h1>
<p>Sorry, Java has thrown an exception.</p>
<p>To continue, click the Back button.</p>

<h2>Details</h2>
<p>Type: ${pageContext.exception["class"]}</p>
<p>Message: ${pageContext.exception.message}</p>

</body>
    
<jsp:include page="MCSDFooter.jsp" />
