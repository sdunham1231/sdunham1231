<%-- 
    Document   : MCSDBanner
    Created on : Jan 24, 2018, 1:01:06 PM
    Author     : Mcorsetti0469
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="./styles/main.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MCSD Computer Programming Club</title>
    </head>
    <body>
        <header>
            <img src="./images/mcsd-icon.png" alt="Team Logo" 
                 style="widrh:75px;height:75px;"/>
            <h1>MCSD Computer Programming Club</h1>
            <h2>IT@Conestoga</h2>
        </header>
        <nav id="nav_bar">
                <ul>
                    <li><a href="MCSDIndex.jsp">Home</a></li>
                    <li><a href="MCSDRegister.jsp">Register</a></li>
                    <li><a href="MCSDLoan">eLoan</a></li>
                    <li><a href="MCSDCart">eCart</a></li>
                    <li><a href="MCSDAdmin.jsp">Admin</a></li>
                </ul>
            </nav>
