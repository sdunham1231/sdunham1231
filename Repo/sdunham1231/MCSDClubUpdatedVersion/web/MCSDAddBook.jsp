<%-- 
    Document   : MCSDAddBook
    Created on : Feb 21, 2018, 1:34:03 PM
    Author     : sdunham1231
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<jsp:include page="MCSDBanner.jsp" />

<section>
        <h1>Add a Book</h1>
        <form method="POST" action="MCSDAddBook">
            <%
            if(request.getAttribute("message") != null && request.getAttribute("message") != "") {
                out.println("<p style=\"color: red\"><i>" + request.getAttribute("message") + "</i></p>");
            }    
            %>
            <table>
        <tr>
            <td><b>Code: </b></td>
            <td><input type="text" name="code" size="10"></td>
        </tr>
         
        <tr>
            <td><b>Description: </b></td>
            <td><input type="text" name="description" size="35"></td>
        </tr>
         
        <tr>
            <td><b>Quantity: </b></td>
            <td><input type="text" name="quantity" size="5"></td>
        </tr>
          
        <tr>
            <td></td>
            <td><button type="submit" value="save">Save</button>
            <button type="submit" value="cancel" onclick="form.action='MCSDDisplayBooks'">Cancel</button></td>
        </tr>
         </table>
        </form>


</section>

<jsp:include page="MCSDFooter.jsp" />