<%-- 
    Document   : MCSDECart
    Created on : Apr 3, 2018, 2:23:37 PM
    Author     : Mcorsetti0469
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<jsp:include page="MCSDBanner.jsp" />

<section>
     <table>
         <caption><b>Your Loan Cart</b></caption>
            <tr>
            <th>
                <b>Code</b>
            </th>
            <th>
                <b>Description</b>
            </th>
            <th align="right">
                <b>Quantity</b>
            </th>
            </tr>
            <c:set var="quantityToal" value="${0}"/>
            <c:forEach var="item" items="${cart.items}">
                <tr>
                <td><c:out value="${item.code}" /></td>
                <td><c:out value="${item.description}" /></td>
                <td align="right"><c:out value="${item.quantity}" /></td>
                <td></td>
                <c:set var = "quantityTotal" value="${quantityTotal + item.quantity}"/>
                    
                </tr>
                
            </c:forEach>
                <td></td>
                <td align="right">Total:</td>
                <td align="right"><c:out value = "${quantityTotal}"/></td>
        </table>
        
                <a href="MCSDClearCart">Clear the cart</a><br>
                <a href="MCSDELoan.jsp">Return to eLoan</a>
</section>


<jsp:include page="MCSDFooter.jsp" />
