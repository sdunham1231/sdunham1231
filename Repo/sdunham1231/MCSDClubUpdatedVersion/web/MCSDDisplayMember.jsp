<%-- 
    Document   : MCSDDisplayMember
    Created on : Jan 31, 2018, 11:24:15 AM
    Author     : Mcorsetti0469
--%>

<jsp:include page="MCSDBanner.jsp" />

<section>
    <h1><b>Thanks for joining our club!</b></h1>
    
    <p><b>Here is the information you entered:</b></p><br>
    <b>Full Name:</b> ${param.firstName} <br><br>
    <b>Email:</b> ${param.email} <br><br>
    <b>Phone:</b> ${param.phone} <br><br>
    <b>IT Program:</b> ${param.program} <br><br>
    <b>Year Level:</b> ${param.year} <br><br>
    
    <p>To register another member, click on the Back button in your browser
        or the Return button shown below.</p>
    
    <form action="MCSDRegister.jsp" get="post">
        <button type="submit" value="Submit">Return</button> 
    </form>
    
</section>
    
<jsp:include page="MCSDFooter.jsp" />
