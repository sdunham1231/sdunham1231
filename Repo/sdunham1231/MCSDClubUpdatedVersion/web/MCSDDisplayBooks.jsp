<%-- 
    Document   : MCSDDisplayBooks
    Created on : Feb 21, 2018, 12:48:53 PM
    Author     : sdunham1231
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="MCSDBanner.jsp" />

    <body>
        <h1>List of Books</h1>
        <table>
            <tr>
            <th>
                Code
            </th>
            <th>
                Description
            </th>
            <th>
                Quantity
            </th>
            </tr>
            
            <c:forEach var="item" items="${books}">
                <tr>
                <td><c:out value="${item.code}" /></td>
                <td><c:out value="${item.description}" /></td>
                <td><c:out value="${item.quantity}" /></td>
                </tr>
                
            </c:forEach>
        </table>
        <form action="MCSDAddBook.jsp" get="post">
            <button type="submit" value="AddBook">Add Book</button>
        </form>
    </body>

<jsp:include page="MCSDFooter.jsp" />