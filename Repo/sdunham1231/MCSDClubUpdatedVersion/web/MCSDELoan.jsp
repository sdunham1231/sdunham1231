<%-- 
    Document   : MCSDELoan
    Created on : Apr 3, 2018, 1:26:23 PM
    Author     : Mcorsetti0469
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<jsp:include page="MCSDBanner.jsp" />

<section>
    
         <table>
            <tr>
            <th>
                <b>Code</b>
            </th>
            <th>
                <b>Description</b>
            </th>
            <th>
                <b>QOH</b>
            </th>
            <th>
                <b>Action</b>
            </th>
            </tr>
            
            <c:forEach var="item" items="${loanitems}">
                <tr>
                <td><c:out value="${item.code}" /></td>
                <td><c:out value="${item.description}" /></td>
                <c:choose>
                    <c:when test="${item.quantity == 0}">
                        <td><c:out value="${item.quantity}" /></td>
                        <td align="right">N/A</td>
                    </c:when>
                    <c:when test="${item.quantity > 0}">
                        <td><c:out value="${item.quantity}" /></td>
                        <td><a href="MCSDCart?action=reserve&code=${item.code}">Reserve</a></td>
                    </c:when>
                </c:choose>
                </tr>
            </c:forEach>
        </table>
   
   
</section>

<jsp:include page="MCSDFooter.jsp" />
