package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class MCSDRegister_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write('\n');
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "MCSDBanner.jsp", out, false);
      out.write("\n");
      out.write("\n");
      out.write("<section>\n");
      out.write("    <h1>New Member Registration Form</h1>\n");
      out.write("    \n");
      out.write("    <form action=\"MCSDDisplayMember.jsp\" get=\"post\">\n");
      out.write("        First Name: <input type=\"text\" name=\"firstName\"><br>\n");
      out.write("        Email: <input type=\"text\" name=\"email\"><br>\n");
      out.write("        Phone: <input type=\"text\" name=\"phone\"><br>\n");
      out.write("        IT Program: <select>\n");
      out.write("            <option value=\"CAD\">CAD</option>\n");
      out.write("            <option value=\"CP\">CP</option>\n");
      out.write("            <option value=\"CPA\">CPA</option>\n");
      out.write("            <option value=\"ITID\">ITID</option>\n");
      out.write("            <option value=\"ITSS\">ITSS</option>\n");
      out.write("            <option value=\"MSD\">MSD</option>\n");
      out.write("            <option value=\"Other\">Other</option>\n");
      out.write("        </select><br>\n");
      out.write("        Year Level: <select>\n");
      out.write("            <option value=\"1\">1</option>\n");
      out.write("            <option value=\"2\">2</option>\n");
      out.write("            <option value=\"3\">3</option>\n");
      out.write("            <option value=\"4\">4</option>\n");
      out.write("        </select><br>\n");
      out.write("        \n");
      out.write("        <button type=\"submit\" value=\"Submit\">Register Now!></button>\n");
      out.write("        <button type=\"reset\" value=\"Reset\">Reset></button>\n");
      out.write("        \n");
      out.write("    </form>\n");
      out.write("</section>\n");
      out.write("     \n");
      out.write("\n");
      org.apache.jasper.runtime.JspRuntimeLibrary.include(request, response, "MCSDFooter.jsp", out, false);
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
