<%-- 
    Document   : MCSDRegister
    Created on : Jan 31, 2018, 11:11:29 AM
    Author     : Mcorsetti0469
--%>
<jsp:include page="MCSDBanner.jsp" />

<section>
    <h1>New Member Registration Form</h1><br>
    
    <form action="MCSDDisplayMember.jsp" get="post">
        <b>First Name:</b> <input type="text" name="firstName" required><br><br>
        <b>Email:</b> <input type="text" name="email" required><br><br>
        <b>Phone:</b> <input type="text" name="phone"><br><br>
        <b>IT Program:</b> <select name="program" text-align="right">
            <option value="CAD">CAD</option>
            <option value="CP">CP</option>
            <option value="CPA">CPA</option>
            <option value="ITID">ITID</option>
            <option value="ITSS">ITSS</option>
            <option value="MSD">MSD</option>
            <option value="Other">Other</option>
        </select><br><br>
        <b>Year Level:</b> <select name="year" text-align="right">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
        </select><br><br>
        
        <button type="submit" value="Submit">Register Now!</button>
        <button type="reset" value="Reset">Reset</button>
        
    </form>
</section>
     

<jsp:include page="MCSDFooter.jsp" />