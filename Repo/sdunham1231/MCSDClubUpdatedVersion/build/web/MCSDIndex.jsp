<%-- 
    Document   : MCSDIndex
    Created on : Jan 24, 2018, 1:31:34 PM
    Author     : Mcorsetti0469
--%>
<jsp:include page="MCSDBanner.jsp" />

<section>
    
    <h2>Java Web Technologies: Section 3</h2><br>
    
    <p>Pair Programming Team</p><br>
    
    <h2>Assignment 1</h2><br>
    
    <h2>Driver: Matt Corsetti</h2>
    
    <h2>Observer: Sean Dunham</h2><br>
    
    <p>Current Date and Time</p><br>
    
    <h2><%=new java.util.Date()%></h2>
    
</section>

<jsp:include page="MCSDFooter.jsp" />
