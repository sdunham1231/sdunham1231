/*
    Document   : MCSDCartServlet
    Created on : April 3, 2018, 1:50 PM
    Author     : Mcorsetti0469
 */
package club.cart;

import club.business.Book;
import club.business.ECart;
import club.business.ELoan;
import java.io.Console;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Mcorsetti0469
 */
public class MCSDCartServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
            ServletContext sc = getServletContext();
            
            String action = request.getParameter("action");
            
            // if action is null, set action to default action
            if (action == null) 
            {
                action = "reserve";  
            }
            
            else if (action.equals("reserve"))
            {
                String code = request.getParameter("code");
                
                
                HttpSession session = request.getSession();
                
                ArrayList<Book> loanitems = (ArrayList<Book>) sc.getAttribute("loanitems");
            
                ECart cart = (ECart) session.getAttribute("cart");
                
                if (cart == null) {
                cart = new ECart();
            }
                
                session.setAttribute("cart", cart);
                
                session.setAttribute("code", code);
                
                cart.addItem(ELoan.findItem(loanitems, code));

                ELoan.subtractFromQOH(loanitems, code, 1);
                
                
            }

            
            getServletContext()
                .getRequestDispatcher("/MCSDECart.jsp")
                .forward(request, response);
        
            
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
