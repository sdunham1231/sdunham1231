/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package club.admin;

import club.business.Book;
import club.data.BookIO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author sdunham1231
 */
public class MCSDAddBookServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = "";
        
        
            String code = request.getParameter("code");
            String description = request.getParameter("description");
            String quantityString = request.getParameter("quantity");
            int quantity = Integer.parseInt(quantityString);
            
            Boolean isValid = true;
            
            Book book = new Book();
            
            
            String message = "";
            if (code == null || code.isEmpty()) {
                message += "Book code is required.<br>";
                url = "/MCSDAddBook.jsp";
                isValid = false;
            }
            if (description == null || description.isEmpty() || description.length() < 2) {
                message += "Description must be at least 2 characters.<br>";
                url = "/MCSDAddBook.jsp";
                isValid = false;
            }
            if (quantity <= 0) {
                message += "Quantity must be a positive number.";
                url = "/MCSDAddBook.jsp";
                isValid = false;
            }
            
            if (isValid == true) {
                url = "/MCSDDisplayBooks";
                ServletContext context = getServletContext();
                String path = context.getRealPath("/WEB-INF/books.txt");
                book.setCode(code);
                book.setDescription(description);
                book.setQuantity(quantity);
                BookIO.insert(book, path);
            }
            request.setAttribute("book", book);
            request.setAttribute("message", message);
        
        
        getServletContext()
                    .getRequestDispatcher(url)
                    .forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
